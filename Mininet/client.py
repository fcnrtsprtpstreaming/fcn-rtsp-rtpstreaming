import socket
import sys
import threading
import time
import io
import PIL
import PIL.Image
import PIL.ImageTk
import RTPpacket
import VideoStream


from Tkinter import *

v = None
app = None
main_client = None
playback_thread = None
fps = 30.0
### COMMAND LINE USAGE:
# "python client.py [Server hostname] [Server RTSP listening port] [Video file requested]"

class StatusCode:
    OK = 200
    NOT_FOUND = 404

class State:
    INIT = 0
    READY = 1
    PLAYING = 2
    TEARDOWN = 3

class Client:

    def __init__(self):
        global main_client
        main_client = self
        # RTP Variables
        self.rcvdp = None                           # DatagramPacket rcvdp; UDP packet received from the server
        self.RTPsocket = None                       # DatagramSocket RTPsocket; socket to be used to send and receive UDP packets
        self.RTP_RCV_PORT = 25000                  # port where the client will receive the RTP packets
        # RTSP variables
        self.state = State.INIT                     # State is either INIT, READY, or PLAYING
        self.RTSPsocket = None                      # Socket RTSPsocket; socket used to send/receive RTSP messages
        self.video_filename = "movie.mjpeg"         # video file to request to the server
        self.RTSPSeqNb = 0                          # Sequence number of RTSP messages within the session
        self.RTSPid = 0                             # ID of the RTSP session (given by the RTSP Server)
        self.CRLF = "\r\n"
        self.MJPEG_TYPE = 26                        # RTP payload type for MJPEG video
        self.buf = []
        self.RTPPacketHeader = bytearray(12)
        self.seqNum = 0
        self.frameNum = 0
        self.lostPackets = 0
	self.host = None
        # Found in Java file, but not listed here:
        # Buffered Reader/Writer RTSPBufferedReader/Writer
            # static BufferedReader RTSPBufferedReader;
            # static BufferedWriter RTSPBufferedWriter;
        # Timer
        #TODO: do we need to create this timer here?
        self.t = threading.Timer(0.20, self.timer_update)

    # TODO
    def send_RTSP_request(self,request_type,url=None,host=None,port=None,play_range=None):
        protocol = 'RTSP/1.0'
        seq_str = 'CSeq: ' + str(self.RTSPSeqNb)
        transport_str = 'Transport: ' + 'RTP/AVP;unicast;client_port=' + str(self.RTP_RCV_PORT)
        session_str = 'Session: ' + str(self.RTSPid)

        if (not url):
            url = "./movie.mjpeg"

        msg = request_type + " " + url + " " + protocol + '\n'
        msg += seq_str + '\n'

        if (request_type == 'SETUP'):
            if (host==None):
                self.host = '10.0.0.1'
            if (port==None):
                port = self.RTSP_server_port

            address = (self.host,port)
            print "address : ", address
            self.RTSPsocket.connect(address)

            msg += transport_str

        elif (request_type == 'PLAY'):
            if (play_range):
                msg += play_range + '\n'
            msg += session_str + '\n'

        elif (request_type == 'PAUSE'):
            msg += session_str + '\n'

        elif (request_type == 'TEARDOWN'):
            msg += session_str + '\n'

        try:
            res = self.RTSPsocket.sendall(msg)
            if (res):
                print "sending RTSP",request_type, "request failed."
        except Exception as e:
            print e


    # TODO
    def parse_server_response(self):
        reply_code = 0
        try:
            print "waiting for status from server"
            status_line = self.RTSPsocket.recv(300)
            #self.RTPsocket.connect(('localhost',25000))
            print status_line
            return StatusCode.OK
        except Exception as e:
            print e

    def timer_update(self):
        print "timer_update tick"
        # if (self.state == State.PLAYING):
        if (True):
            try:
		print "about to receive RTP"
                data = self.RTPsocket.recv(16000)
		print "received RTP"
                self.RTPPacketHeader = data[:12]
                # print self.RTPPacketHeader[2]
                # print self.RTPPacketHeader[3]

                # int(bytes.encode('hex'), 16)
                self.frameNum = int(self.RTPPacketHeader[2].encode('hex'), 16) << 8 | int(
                    self.RTPPacketHeader[3].encode('hex'), 16)
                print "frameNum", self.frameNum
                self.seqNum += 1
                print "seqNum", self.seqNum
                self.buf.append(data[12:])
                print "len of buf:", len(self.buf)

               # if (len(self.buf) > 100):
               #     self.send_RTSP_request('PAUSE',host=self.host)
               #     time.sleep(2.0)
               #     self.send_RTSP_request('PLAY',host=self.host)

                if self.seqNum != self.frameNum:
                    self.lostPackets += 1
                    print "packet lost : ", self.frameNum
            except Exception as e:
                print "Exception in timer_update", e


            if (self.state == State.TEARDOWN):
                self.t.cancel()
                app.destroy()
                sys.exit(0)
            else:
                self.t = threading.Timer(0.01,self.timer_update)
                self.t.start()



def setup_gui(client):
    global app
    class Application(Frame):
        def setup_command(self):
            print "SETUP"
            setup(self.client)
        def play_command(self):
            print "PLAY"
            play(client)
        def pause_command(self):
            print "PAUSE"
            pause(client)
        def teardown_command(self):
            print "TEARDOWN"
            teardown(client)
        def create_buttons(self):
            self.buttons = list()
            order = ["setup", "play", "pause", "teardown"]
            buttons = {"setup": self.setup_command,
                       "play": self.play_command,
                       "pause": self.pause_command,
                       "teardown": self.teardown_command
                       }
            for text in order:
                btn = Button(self,highlightthickness=1,borderwidth=0)
                btn.config(highlightbackground='blue')
                btn["text"] = text.upper()
                btn["command"] = buttons[text]
                image = PIL.ImageTk.PhotoImage(file=text.upper()+".jpg")
                btn.config(image=image)
                btn.image = image
                btn.pack({"side": "left"})
                self.buttons.append(btn)
        def createWidgets(self):
            # setup_button = Button(self)
            # setup_button["text"] = "SETUP"
            # setup_button["command"] = self.setup_command
            # setup_button.pack({"side":"left"})
            # self.setup_button = setup_button
            self.create_buttons()

            image = PIL.Image.open('image1.jpg')
            photo = PIL.ImageTk.PhotoImage(image)
            label = Label(image=photo,highlightthickness=0,borderwidth=0)
            label.image = photo  # keep a reference!
            label.pack(pady=10)
            self.label = label


        def __init__(self, master=None,client=client):
            Frame.__init__(self, master)
            self.client = client
            self.pack()
            self.createWidgets()
    root = Tk()
    root.configure(background='black')
    app = Application(master=root,client=client)
    app.master.title("Client")
    app.master.minsize(550,325)
    root.resizable(width=False, height=False)
    root.geometry('{}x{}'.format(550,325))
    app.mainloop()
    root.destroy()

def main():
    client = Client()

    if (len(sys.argv) <= 1):
        client.RTSP_server_port = 25001
        client.server_host = '10.0.0.1'
    else:
        client.RTSP_server_port = int(sys.argv[2])
        client.server_host = sys.argv[1]

    #RTSP_HOST = socket.gethostbyname(client.server_host)

    #print "RTSP_HOST" + RTSP_HOST

    # if a video filename is provided, use it. Otherwise default is
    # movie.mjpeg
    if (len(sys.argv) >= 4):
        client.video_filename = sys.argv[3]

    client.RTSPsocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    setup_gui(client)

def setup(client):
    # If not the INIT state, setup does nothing.
    if client.state != State.INIT:
        return

    # Init non-blocking RTPsocket that will be used to receive data
    try:
        print "Initiating non-blocking RTPsocket..."
        # construct a new "datagram" socket to receive RTP packets from the server, on port RTP_RCV_PORT
        # set TimeOut value of the socket to 5msec.
        client.RTPsocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        #client.RTPsocket.settimeout(0.005)
        #client.RTPsocket.bind(('localhost',client.RTP_RCV_PORT))
        print 'binding socket to ', client.server_host, client.RTP_RCV_PORT
        client.RTPsocket.bind(('', client.RTP_RCV_PORT))
    except Exception as e:
        print e
        print "Socket Exception"
        sys.exit(0)

    client.RTSPSeqNb = 1                    # init RTSP sequence number

    client.send_RTSP_request("SETUP")   # Send SETUP message to the server

    if (client.parse_server_response() != StatusCode.OK):
        print "Invalid Server Response"
    else:
        client.state = State.READY
        print "Client is now ready"
        pass

def play(client):
    if (client.state == State.READY):
        # TODO
        client.RTSPSeqNb += 1

        # Send PLAY message to the sever
        client.send_RTSP_request("PLAY")

        # Wait for the response
        if (client.parse_server_response() != StatusCode.OK):
            print "Invalid Server Response"
        else:
            # TODO vvvv
            # change RTSP state and print out new state
            client.state = State.PLAYING
            client.timer_update()
            playback()

            pass
    elif (client.state == State.READY):
        print "Going to PLAYING from READY"
        # Send PLAY message to the sever
        
        # easiest way to get restart pl
        play(client)
        
        #client.send_RTSP_request("PLAY")
        #client.state = State.PLAYING
        #client.timer_update()
        #playback()

    else:
        # if client.state != State.READY then do nothing
        print "Client is not READY, cannot PLAY"
        return

def pause(client):
    if (client.state == State.PLAYING):
        # TODO
        # increase RTSP sequence number

        # send PAUSE message to the server
        client.send_RTSP_request("PAUSE")

        # Wait for the response
        if (client.parse_server_response() != StatusCode.OK):
            print "Invalid Server Response"

        else:
            # TODO vvvv
            # change RTSP state and print out new state
            print "Going to READY from PLAYING"
            client.state = State.READY;

            playback_thread.cancel()
            # stop the timer
            # we must configure timing - as of now I am not sure of
            # the purpose of the timer
            pass
    else:
        # if client.state != State.PLAYING then do nothing
        return

def teardown(client):

    print "Teardown initiated"
    client.RTSPSeqNb += 1

    # send TEARDOWN message to the server
    client.send_RTSP_request("TEARDOWN")

    # Wait for the response
    if (client.parse_server_response() != StatusCode.OK):
        print "Invalid Server Response"

    else:
        client.state = State.TEARDOWN
        print "client state = TEARDOWN"

        #shutdown and close the rtp socket connection
        #client.RTPsocket.shutdown(socket.SHUT_RDWR)
        client.RTPsocket.close()
        print "client RTP Port closed"

        client.t.cancel()
        playback_thread.cancel()

        
        # Exit the client
        #sys.exit(0)
    print "total lost packets : ", client.lostPackets
    print "loss rate : ", client.lostPackets/float(client.frameNum)

photo = None
image_data = None
label = None

def playback():
    global playback_thread
    playback_thread = threading.Timer(1.0 / 30.0, playback)

    global image_data
    global photo
    global label
    
    if (main_client.state == State.PLAYING):
        if (len(main_client.buf)):
            frame = main_client.buf[0]
            main_client.buf = main_client.buf[1:]
            if (frame):
                image_data = frame
                image = PIL.Image.open(io.BytesIO(image_data))

                app.label.destroy()
                photo = PIL.ImageTk.PhotoImage(image)
                label = Label(image=photo, highlightthickness=0, borderwidth=0)
                label.image = photo  # keep a reference!
                label.pack(pady=10)
                app.label = label
        else:
            app.destroy()
            sys.exit(0)



    if main_client.state == State.PLAYING:
        playback_thread.start()



if __name__ == '__main__':
    main()
