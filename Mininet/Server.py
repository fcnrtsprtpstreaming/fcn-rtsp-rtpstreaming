import socket
import sys
import random
import threading
import time
import RTPpacket
import VideoStream

class Server:
	#RTSP variables
	#rtsp states
	INIT = 0
	READY = 1
	PLAYING = 2

	#rtsp message types
	SETUP = 'SETUP'
	PLAY = 'PLAY'
	PAUSE = 'PAUSE'
	TEARDOWN = 'TEARDOWN'

	state = INIT

	rtspConnection = {}
        counter = 0

	def start(self):
		#create an RTSP port to listen and connect to clients
		serverPort = int(sys.argv[1])
		rtspSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		rtspSocket.bind(('', serverPort))
		#rtspSocket.bind(("172.24.25.146", serverPort))
		print "Listening for an incoming RTSP request..."
		rtspSocket.listen(5)
		self.rtspConnection['rtspSocket'] = rtspSocket.accept()
		print "Accetped a client's connection", self.rtspConnection
		threading.Thread(target=self.startReceivingRTSPRequests).start()

	def startReceivingRTSPRequests(self):
		self.clientSocket = self.rtspConnection['rtspSocket'][0]
		while True:
			request = self.clientSocket.recv(500)
			if request:
				print "Request Received:",'\n',request
				self.processRTSPRequest(request)


	def processRTSPRequest(self, aRequest):
		## sample RTSP request format
		"""
		SETUP rtsp://example.com/media.mp4/streamid=0 RTSP/1.0
    	CSeq: 3
      	Transport: RTP/AVP;unicast;client_port=8000
      	"""

		aRequest = aRequest.strip() # get rid of extraneous newlines and whitespace
		request_lines = aRequest.split('\n') # each entry is a separate line of the RTSP request

		header = 		request_lines[0].split()
		requestType = 	header[0]
		filename =		header[1]
		protocol =		header[2]

		seq_line = 		request_lines[1].split()
		seqNum =		int(seq_line[1])


		"""
		request = aRequest.split('\n')
		line1 = request[0].split(' ')
		requestType = line1[0]
		# Get the media file name
		filename = line1[1]
		# Get the RTSP sequence number
		seqNum = request[1].split(' ')
		"""
		if requestType == self.SETUP:
			if self.state == self.INIT:
				# Update state
				print "SETUP request received\n"
				try:
					self.rtspConnection['videoStream'] = VideoStream.VideoStream(filename)
					self.state = self.READY

				except IOError:
					self.sendRTSPReply("404 NOT FOUND", seqNum)


				transport_line = request_lines[2].split()[1].split(';') # get what's after "Transport:"
				rtp_port = int(transport_line[2].split('=')[1])


				# Generate a random RTSP session ID
				self.rtspConnection['session'] = random.randint(100000, 999999)

				# Send RTSP reply
				self.sendRTSPReply("200 OK", seqNum)
				print "seqNum is", seqNum
				# Get the RTP/UDP port from the last line
				self.rtspConnection['rtpPort'] = rtp_port
				print "\nrtpPort is:", self.rtspConnection['rtpPort']
				print "filename is:", filename


		# Process PLAY request
		elif requestType == self.PLAY:
			if self.state == self.READY:
				print "PLAY request received\n"
				self.state = self.PLAYING

				# Create a new socket for RTP/UDP
				self.rtspConnection["rtpSocket"] = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
				self.rtspConnection["rtpSocket"].connect(("10.0.0.2",25000))
				self.sendRTSPReply("200 OK", seqNum)
				print "Sequence Number (", seqNum, ")\nReplied to client\n"

				# Create a new thread and start sending RTP packets
				self.rtspConnection['event'] = threading.Event()
				self.rtspConnection['worker']= threading.Thread(target=self.sendRTPPacket)
				self.rtspConnection['worker'].start()

		# Process PAUSE request
		elif requestType == self.PAUSE:
			if self.state == self.PLAYING:
				print "\nPAUSE Request Received\n"
				self.state = self.READY
				self.rtspConnection['event'].set()
				self.sendRTSPReply("200 OK", seqNum)

		# Process TEARDOWN request
		elif requestType == self.TEARDOWN:
			print "\nTEARDOWN Request Received\n"
			self.rtspConnection['event'].set()
			self.sendRTSPReply("200 OK", seqNum)
			# Close the RTP socket
			self.rtspConnection['rtpSocket'].close()
			#sys.exit(0)


	def sendRTSPReply(self, aCode, aSeqNum):
		# Send RTSP reply to the client.
		if aCode == "200 OK":
			print "200 OK"
			reply = 'RTSP/1.0 200 OK\nCSeq: ' + str(aSeqNum) + '\nSession: ' + str(self.rtspConnection['session'])
			connectedSocket = self.rtspConnection['rtspSocket'][0]
			connectedSocket.send(reply)
		# Error messages
		elif aCode == "404 NOT FOUND":
			print "404 NOT FOUND"
		elif aCode == "500 CONNECTION ERROR":
			print "500 CONNECTION ERROR"

	def makeRTPPacket(self, payload, frameNbr):
		# RTP-packetize the video data
		version = 2
		padding = 0
		extension = 0
		cc = 0
		marker = 0
		pt = 26 # MJPEG type
		seqnum = frameNbr
		ssrc = 0

		rtpPacket = RTPpacket.RTPpacket(version, padding, extension, cc, seqnum, marker, pt, ssrc, int(time.time()), payload)

		#rtpPacket.encode(version, padding, extension, cc, seqnum, marker, pt, ssrc, int(time), payload)

		return rtpPacket.getPacket()


	def sendRTPPacket(self):
		# Send RTP packets over UDP
		#counter = 0
		threshold = 10


		while True:
			# Stop sending if request is PAUSE or TEARDOWN
			if self.rtspConnection['event'].isSet():
				break

			data = self.rtspConnection['videoStream'].getNextFrame()
			print "\ndata from nextFrame():\n" #+ data + "\n"
			if data:
				frameNumber = self.rtspConnection['videoStream'].getFrameNum()
				# try:
				port = int(self.rtspConnection['rtpPort'])

				packet = self.makeRTPPacket(data,frameNumber)

				#self.rtspConnection['rtpSocket'].sendto(packet, \
				#	(self.rtspConnection['rtspSocket'][1][0],port))
				self.rtspConnection['rtpSocket'].send(packet)
				self.counter += 1
				time.sleep(0.02)
				print "packet sent:", self.counter
                if(self.counter >= 1000):
                    break;
				# except Exception as e:
				# 	print e




if __name__ == "__main__":
	Server().start()

