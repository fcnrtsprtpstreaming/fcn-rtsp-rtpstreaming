import time

def main(arg):
	print 'hello world!'
	if arg == 'OK':
		print 'ok'
	while True:
		print "going to sleep"
		time.sleep(.06)
		print "woke up"


if __name__ == "__main__":
	main("OKay")