import struct

def int2bytes(integer,width=2):
    if width == 2:
        return struct.pack('>H',integer)
    if width == 4:
        return struct.pack('>I',integer)
    print "Invalid argument passed to int2bytes"
    return None


def b2int(self, bytes):
    return int(bytes.encode('hex'), 16)

class RTPpacket:

    #Size of RTP header
    HEADER_SIZE = 12

    def __init__(self,version, padding, extension, cc, Framenb, marker, PType, ssrc, Time, data):
        # fill by default heacketder fields:
        self.Version = version
        self.Padding = padding
        self.Extension = extension
        self.CC = cc
        self.Marker = marker
        self.Ssrc = ssrc

        # fill changing header fields:
        self.SequenceNumber = Framenb
        self.TimeStamp = Time
        self.PayloadType = PType

        # build the header bistream:
        self.header = bytearray(RTPpacket.HEADER_SIZE)  # Creating array of bytes
        # fill the header array of byte with RTP header fields


        first_byte = self.Version << 6
        first_byte |= self.Padding << 5
        first_byte |= self.Extension << 4
        first_byte |= self.CC

        second_byte = self.Marker << 7
        second_byte |= self.PayloadType

        # entries in a bytearray can be directly assigned via array[i] = 0 <= number <= 255
        # however, if a number takes more than one byte, we must get the bytestring first.
        # int2bytes(integer, width) does this (it calls struct.pack)
        # then, usage is array[index:index+width] = int2bytes(number,width)
        # width can be either 2 or 4 (shorts or ints).

        self.header[0] = first_byte
        self.header[1] = second_byte
        self.header[2:4] = int2bytes(self.SequenceNumber, width=2)
        self.header[4:8] = int2bytes(self.TimeStamp, width=4)
        self.header[8:12] = int2bytes(self.Ssrc, width=4)

        # fill the payload bitstream:
        # self.payload_size = data_length
        # self.payload = bytearray(data_length)
        self.payload = data
        # fill payload array of byte from data (given in parameter of the constructor)

        # this is going to be slow probably
        # for i in range(0, data_length):
        # self.payload[i] = data[i]

    #Constructor of an RTPpacket object from header fields and payload bitstream
    def encode(self,version, padding, extension, cc, Framenb, marker, PType, ssrc, Time, data):
        print "encode : RTPPacket"

        #fill by default heacketder fields:
        self.Version = version
        self.Padding = padding
        self.Extension = extension
        self.CC = cc
        self.Marker = marker
        self.Ssrc = ssrc

        #fill changing header fields:
        self.SequenceNumber = Framenb
        self.TimeStamp = Time
        self.PayloadType = PType

        #build the header bistream:
        header = bytearray(RTPpacket.HEADER_SIZE) # Creating array of bytes
        #fill the header array of byte with RTP header fields


        first_byte = self.Version << 6
        first_byte |= self.Padding << 5
        first_byte |= self.Extension << 4
        first_byte |= self.CC

        second_byte = self.Marker << 7
        second_byte |= self.PayloadType

        # entries in a bytearray can be directly assigned via array[i] = 0 <= number <= 255
        # however, if a number takes more than one byte, we must get the bytestring first.
        # int2bytes(integer, width) does this (it calls struct.pack)
        # then, usage is array[index:index+width] = int2bytes(number,width)
        # width can be either 2 or 4 (shorts or ints).

        header[0] = first_byte
        header[1] = second_byte
        header[2:4] = int2bytes(self.SequenceNumber,width=2)
        header[4:8] = int2bytes(self.TimeStamp,width=4)
        header[8:12] = int2bytes(self.Ssrc, width=4)

        #fill the payload bitstream:
        #self.payload_size = data_length
        #self.payload = bytearray(data_length)
        self.payload = data
        #fill payload array of byte from data (given in parameter of the constructor)

        # this is going to be slow probably
        #for i in range(0, data_length):
            #self.payload[i] = data[i]

    def decode(self,data):
        self.header = bytearray(data[:12])
        self.payload =  data[12:]
        return self.payload

    #Constructor of an RTPpacket object from the packet bistream
    @classmethod # Since constructor overloading not possible in Python - changes to be made in other classes accordingly
    def constructorTwo(cls, packet, packet_size):

        self.Version = 2
        self.Padding = 0
        self.Extension = 0
        self.CC = 0
        self.Marker = 0
        self.Ssrc = 0

        #check if total packet size is lower than the header size
        if(packet_size >= RTPpacket.HEADER_SIZE):

            #get the header bitsream:
            self.header = bytearray(RTPpacket.HEADER_SIZE)
            for i in range(0, RTPpacket.HEADER_SIZE):
                self.header[i] = packet[i]

            #get the payload bitstream:
            self.payload_size = packet_size - RTPpacket.HEADER_SIZE
            self.payload = bytearray(self.payload_size)

            for i in range(RTPpacket.HEADER_SIZE, packet_size):
                self.payload[i - RTPpacket.HEADER_SIZE] = packet[i]

            #interpret the changing fields of the header:
            self.PayloadType = self.header[1] & 127
            self.SequenceNumber = unsigned_int(self.header[3]) + 256*unsigned_int(self.header[2])
            self.TimeStamp = unsigned_int(self.header[7]) + 256*unsigned_int(self.header[6]) + 65536*unsigned_int(self.header[5]) + 16777216*unsigned_int(self.header[4]);


    #getpayload: return the payload bitstream of the RTPpacket and its size
    def getpayload(self,data):
           return self.payload_size

    #getpayload_length: return the length of the payload
    def getpayload_length(self):
        return self.payload_size

    #getlength: return the total length of the RTP packet
    def getlength(self):
        return (self.payload_size + RTPpacket.HEADER_SIZE)

    #getpacket: returns the packet bitstream and its length
    def getPacket(self):

        # for i in range(0, RTPpacket.HEADER_SIZE):
        #     packet[i] = self.header[i]
        #
        # for i in range(0, self.payload_size):
        #     packet[i + RTPpacket.HEADER_SIZE] = self.payload[i]

        # return (self.payload_size + RTPpacket.HEADER_SIZE)
        return (self.header + self.payload)

    #gettimestamp
    def gettimestamp(self):
        return self.TimeStamp

    #getsequencenumber
    def getsequencenumber(self):
        return self.SequenceNumber

    #getpayloadtype
    def getpayloadtype(self):
        return self.PayloadType

    #print headers without the SSRC
    def printheader(self):

        for i in range(0, (RTPpacket.HEADER_SIZE-4)):

            for j in range(7,-1,-1): # for(int j = 7; j >=0; j--)
                if(((1 << j) & self.header[i] ) != 0):
                    print "1"
                else:
                    print "0"
                print " "

        print # Print empty line

    @staticmethod
    def unsigned_int(nb): # STATIC METHOD
        if(nb >= 0):
            return nb
        else:
            return (256 + nb)



