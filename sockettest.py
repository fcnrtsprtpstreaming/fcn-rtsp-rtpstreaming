#!/usr/bin/python

from mininet.topo import Topo, SingleSwitchTopo
from mininet.net import Mininet
from mininet.log import lg, info
from mininet.cli import CLI
from mininet.link import TCLink
from mininet.log import setLogLevel, info
from mininet.node import Controller, RemoteController


def main():
    lg.setLogLevel('info')

    net = Mininet(topo = None, build = False)
    info('***Adding controller\n')
    net.addController(name = 'c0')
    info('***** Adding switches\n')
    s1 = net.addSwitch('s1')

    info('***** Add hosts\n')
    h1 = net.addHost('h1')
    h2 = net.addHost('h2')

    net.addLink(h1, s1, cls = TCLink, bw = 10, delay = '1ms', loss = 1)
    net.addLink(h2, s1, cls = TCLink, bw = 10, delay = '1ms', max_queue_size=1000, loss = 10)
    info('*** Starting network\n')

    net.start()

    CLI( net )
    #p1.terminate()
    net.stop()

if __name__ == '__main__':
    main()
