#from java.io import FileInputStream
import struct
import RTPpacket
from RTPpacket import b2int, int2bytes

def b2int(bytes):
    return int(bytes.encode('hex'), 16)


class VideoStream:

    #Constructor
    def __init__(self,filename):
        print "__init__ : videostream"
        self.fis = open(filename, "rb")
        self.frameNum = 0
        ### This line moves the file cursor forward by 10 vvvvv
        # print "Here: " ,self.fis.read(10) # Test : not part of original code

    #returns the next frame as an array of byte and the size of the frame
    def getNextFrame(self):
        print "VideoStream: getNextFrame"
        length = 0
        length_String = None
        frame_length = bytearray(5)

        frame_length[0:5] = self.fis.read(5)
        #print frame_length
        length_String = str(frame_length) # Aim: to convert the frame_length(bytearray) to integer
        #length = b2int(bytes(frame_length))

        if length_String == '':
            print "reached EOF, looping from beginning"
            self.fis.seek(0);
            frame_length[0:5] = self.fis.read(5)
            length_String = str(frame_length)

            #return None
        #print length
        frame = self.fis.read(int(length_String))
        self.frameNum += 1
        print "VideoStream:",len(frame)
        return frame

    def __del__(self):
        self.fis.close()

    def getFrameNum(self):
        return self.frameNum

'''
#Testing, not part of original code
if __name__ == "__main__":
    vid = VideoStream('movie.mjpeg') #Testing with dummy video
    f = bytearray(5)
    frame = vid.getNextFrame()

    while(1):
        frame = vid.getNextFrame()

    #print vid.getnextframe(f)
'''